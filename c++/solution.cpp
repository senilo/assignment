#include <iostream>
#include <vector>
#include <string>

/*
	Return the number of finished students, when starting at student x
    x starts at 1
*/
int count_finished(int x, const std::vector<int> &extra_time_needed) {
	int num_finished = 0;
	for (int t = 0; t < extra_time_needed.size(); t++) {
		int student = (x + t - 1) % extra_time_needed.size();
		if (extra_time_needed[student] <= t)
			num_finished++;
	}
	return num_finished;
}

/*
	Return the best starting position to get the most finished students
	The first position is 1
*/
int find_most_finished(const std::vector<int> &extra_time_needed) {
	int max = 0;
	int starting_pos;

	for (int i = 0; i < extra_time_needed.size(); i++) {
		int num_finished = count_finished(i + 1, extra_time_needed);
		if (num_finished > max) {
			max = num_finished;
			starting_pos = i + 1;
		}
	}
	return starting_pos;
}


int main() {
	// Read input
	int n;
	std::cin >> n;
	std::vector<int> extra_time_needed(n);
	for (int i = 0; i < n; i++) {
		std::cin >> extra_time_needed[i];
	}

	std::cout << find_most_finished(extra_time_needed) << std::endl;
}