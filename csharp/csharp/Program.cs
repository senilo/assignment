﻿using System;
using System.Linq;

namespace csharp
{
    class Program
    {
        /// <summary>
        /// Return the number of finished students
        /// </summary>
        static int CountFinished(int x, int[] extraTimeNeeded)
        {
            int numFinished = 0;
            for(int t = 0; t < extraTimeNeeded.Length; t++) {
                int student = (x + t - 1) % extraTimeNeeded.Length;
                if (extraTimeNeeded[student] <= t)
                    numFinished++;
            }
            return numFinished;
        }

        /// <summary>
        /// Return the best starting position for the given students
        /// </summary>
        static int FindMostFinished(int[] extraTimeNeeded)
        {
            var numFinished = new int[extraTimeNeeded.Length];
            for(int i = 0; i < extraTimeNeeded.Length; i++) {
                numFinished[i] = CountFinished(i + 1, extraTimeNeeded);
            }
            int max = numFinished.Max();
            return Array.FindIndex(numFinished, v => v == max) + 1;
        }

        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine().Trim());
            int[] extraTimeNeeded = Console.ReadLine()
                                        .Trim()
                                        .Split()
                                        .Select(s => int.Parse(s))
                                        .ToArray();
            Console.WriteLine(FindMostFinished(extraTimeNeeded));
        }
    }
}
