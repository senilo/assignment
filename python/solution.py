#
# Anton Olason, 2019-03-16
#
import unittest


def count_finished(x, extra_time_needed):
    """Returns the number of finished students, when starting at student x
       x starts at 1"""
    if not 0 < x <= len(extra_time_needed):
        raise ValueError

    num_finished = 0
    num_students = len(extra_time_needed)
    for t in range(num_students):
        student = (x + t - 1) % num_students
        if extra_time_needed[student] <= t:
            num_finished += 1
    return num_finished


def find_most_finished(extra_time_needed):
    """Return the starting student for the teacher to get the highest number of finished students"""
    if not extra_time_needed:
        raise ValueError

    num_finished = [count_finished(i + 1, extra_time_needed) for i in range(len(extra_time_needed))]
    highest = max(num_finished)
    for i, value in enumerate(num_finished):
        if num_finished[i] == highest:
            return i + 1

#
# Main starts here
# There is no error checking on inputs
#
if __name__ == '__main__':
    n = input()
    extra_time_needed = [int(part) for part in input().split()]
    print(find_most_finished(extra_time_needed))

#
# The tests can be run with the command python -m unittest solution -v
#
class TestCountFinished(unittest.TestCase):
    def test_example(self):
        self.assertEqual(2, count_finished(1, [1, 0, 0]))
        self.assertEqual(3, count_finished(2, [1, 0, 0]))
        self.assertEqual(3, count_finished(3, [1, 0, 0]))

    def test_bounds(self):
        with self.assertRaises(ValueError):
            count_finished(0, [1, 0, 0])
        with self.assertRaises(ValueError):
            count_finished(4, [1, 0, 0])

    def test_many_students(self):
        extra_time = [i + 10 for i in range(10000)]
        self.assertEqual(0, count_finished(1, extra_time))
        self.assertEqual(9989, count_finished(9990, extra_time))

class TestFindMostFinished(unittest.TestCase):
    def test_example(self):
        self.assertEqual(2, find_most_finished([1, 0, 0]))
        self.assertEqual(1, find_most_finished([0, 1, 2]))
        self.assertEqual(3, find_most_finished([1, 2, 0]))
    def test_bounds(self):
        with self.assertRaises(ValueError):
            find_most_finished([])

    def test_many_students(self):
        extra_time = [i + 10 for i in range(10000)]
        self.assertEqual(9991, find_most_finished(extra_time))

