from subprocess import Popen, PIPE
import unittest
import sys
from os import path


CPP_SOLUTION_PATH = [path.join('c++', 'Release', 'solution.exe')]
CSHARP_SOLUTION_PATH = [path.join('csharp', 'csharp', 'bin', 'Release', 'solution.exe')]
PYTHON_SOLUTION_PATH = [sys.executable, path.join('python', 'solution.py')]
C_SOLUTION_PATH = [path.join('c', 'c_solution', 'Release', 'c_solution.exe')]

def run_solution(extra_time, program_path):
    """Run the program in given path with the input and return printed integer"""
    input_string = "%d\n%s\n" % (len(extra_time), ' '.join(map(str, extra_time)))

    p = Popen(program_path, stdin=PIPE, stdout=PIPE, shell=True)
    output = p.communicate(input_string.encode())[0]

    return int(output.decode())

class TestPython(unittest.TestCase):
    def test_example(self):
        self.assertEqual(2, run_solution([1, 0, 0], PYTHON_SOLUTION_PATH))
        self.assertEqual(1, run_solution([0, 1, 2], PYTHON_SOLUTION_PATH))
        self.assertEqual(3, run_solution([1, 2, 0], PYTHON_SOLUTION_PATH))
        self.assertEqual(1, run_solution([2, 2, 2], PYTHON_SOLUTION_PATH))
        self.assertEqual(1, run_solution([2, 1, 2], PYTHON_SOLUTION_PATH))
        self.assertEqual(2, run_solution([2, 0, 0, 2, 3], PYTHON_SOLUTION_PATH))

    def test_many_students(self):
        extra_time = [i + 10 for i in range(1000)]
        self.assertEqual(991, run_solution(extra_time, PYTHON_SOLUTION_PATH))

    ## This one is really slow with 100k students, ~30 minutes
    # def test_100k_students(self):
    #     extra_time = [i + 10 for i in range(100000)]
    #     self.assertEqual(99991, run_solution(extra_time, CPP_SOLUTION_PATH))


class TestCpp(unittest.TestCase):
    def test_example(self):
        self.assertEqual(2, run_solution([1, 0, 0], CPP_SOLUTION_PATH))
        self.assertEqual(1, run_solution([0, 1, 2], CPP_SOLUTION_PATH))
        self.assertEqual(3, run_solution([1, 2, 0], CPP_SOLUTION_PATH))
        self.assertEqual(1, run_solution([2, 2, 2], CPP_SOLUTION_PATH))
        self.assertEqual(1, run_solution([2, 1, 2], CPP_SOLUTION_PATH))

    def test_many_students(self):
        extra_time = [i + 10 for i in range(10000)]
        self.assertEqual(9991, run_solution(extra_time, CPP_SOLUTION_PATH))

    def test_100k_students(self):
        extra_time = [i + 10 for i in range(100000)]
        self.assertEqual(99991, run_solution(extra_time, CPP_SOLUTION_PATH))


class TestCSharp(unittest.TestCase):
    def test_example(self):
        self.assertEqual(2, run_solution([1, 0, 0], CSHARP_SOLUTION_PATH))
        self.assertEqual(1, run_solution([0, 1, 2], CSHARP_SOLUTION_PATH))
        self.assertEqual(3, run_solution([1, 2, 0], CSHARP_SOLUTION_PATH))
        self.assertEqual(1, run_solution([2, 2, 2], CSHARP_SOLUTION_PATH))
        self.assertEqual(1, run_solution([2, 1, 2], CSHARP_SOLUTION_PATH))

    def test_many_students(self):
        extra_time = [i + 10 for i in range(10000)]
        self.assertEqual(9991, run_solution(extra_time, CSHARP_SOLUTION_PATH))

    def test_100k_students(self):
        extra_time = [i + 10 for i in range(100000)]
        self.assertEqual(99991, run_solution(extra_time, CSHARP_SOLUTION_PATH))


class TestC(unittest.TestCase):
    def test_example(self):
        self.assertEqual(2, run_solution([1, 0, 0], C_SOLUTION_PATH))
        self.assertEqual(1, run_solution([0, 1, 2], C_SOLUTION_PATH))
        self.assertEqual(3, run_solution([1, 2, 0], C_SOLUTION_PATH))
        self.assertEqual(1, run_solution([2, 2, 2], C_SOLUTION_PATH))
        self.assertEqual(1, run_solution([2, 1, 2], C_SOLUTION_PATH))

    def test_many_students(self):
        extra_time = [i + 10 for i in range(10000)]
        self.assertEqual(9991, run_solution(extra_time, C_SOLUTION_PATH))

    def test_100k_students(self):
        extra_time = [i + 10 for i in range(100000)]
        self.assertEqual(99991, run_solution(extra_time, C_SOLUTION_PATH))


if __name__ == '__main__':
    unittest.main()
