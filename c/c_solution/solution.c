#include "stdio.h"
#include "stdlib.h"


/*
Return the number of finished students, when starting at student x
x starts at 1
*/
int count_finished(int x, const int extra_time_needed[], const size_t num_students) {
	int num_finished = 0;
	for (size_t t = 0; t < num_students; t++) {
		int student = (x + t - 1) % num_students;
		if (extra_time_needed[student] <= t)
			num_finished++;
	}
	return num_finished;
}

/*
Return the best starting position to get the most finished students
The first position is 1
*/
int find_most_finished(const int extra_time_needed[], const size_t num_students) {
	int max = 0;
	int starting_pos;

	for (size_t i = 0; i < num_students; i++) {
		int num_finished = count_finished(i + 1, extra_time_needed, num_students);
		if (num_finished > max) {
			max = num_finished;
			starting_pos = i + 1;
		}
	}
	return starting_pos;
}


int main() {
	// Read input
	size_t n;
	scanf_s("%d", &n);
	int *extra_time_needed = malloc(sizeof(int)*n);

	for (size_t i = 0; i < n; i++) {
		scanf_s("%d", &extra_time_needed[i]);
	}
	printf("%d", find_most_finished(extra_time_needed, n));

	free(extra_time_needed);

}